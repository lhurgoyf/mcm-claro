import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import ViewPersons from '../screens/ViewPersons'
import DetailPerson from '../screens/DetailPerson'

const StackNavigator = createStackNavigator(
  {
    ViewPersons: {
      screen: ViewPersons
    },
    DetailPerson: {
      screen: DetailPerson
    }
  },
  {
    initialRouteName: 'ViewPersons',
    headerMode: 'none',
    mode: 'modal'
  }
)

export default createAppContainer(StackNavigator)