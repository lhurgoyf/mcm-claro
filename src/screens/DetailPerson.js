import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Text } from 'react-native-paper'
import Header from '../components/Header'
import { useState, useEffect } from 'react'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { handleRating } from '../store/swapi';
import axios from 'axios'
import { Rating } from 'react-native-ratings';

function DetailPerson(id, {navigation}) {

   const [personId, setPersonId] = useState([])
   const [personFilms, setPersonFilms] = useState([])

    const identifier = id.navigation.state.params

    useEffect(() => {
      axios.get(`https://swapi.dev/api/people/${identifier}`)
      .then((res) => {
        const response = res.data    
        const films = res.data.films
        if(response) {
          setPersonId(response);
          const promiseArray = films.map((item) => axios.get(item));
          axios.all(promiseArray)
          .then((resp) => {
            let mapResponse = resp.map(resp => resp.data.title)
            setPersonFilms(mapResponse)
          })
          .catch((error) => {
            console.log(error);
          });
        }   
      })
      .catch((error) => {
        console.log(error);
      });
    },[]);

    function ratingCompleted(rating) {
      handleRating(rating)
    }

    return (
      <>
        <Header titleText={personId.name} />
        <View style={styles.container}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>Nome: {personId.name}</Text>
            <Text style={styles.title}>Largura: {personId.height}</Text>
            <Text style={styles.title}>Peso: {personId.mass}</Text>
            <Text style={styles.title}>Cor do cabelo: {personId.hair_color}</Text>
            <Text style={styles.title}>Cor da pele: {personId.skin_color}</Text>
            <Text style={styles.title}>Cor do Olho: {personId.eye_color}</Text>
            <Text style={styles.title}>Gênero: {personId.gender}</Text>
            <Text style={styles.title}>Data de nascimento: {personId.birth_year}</Text>
            <Text style={styles.films}>Filmes:</Text>
            <View>
              {personFilms.map((item, index) => {
                return (
                  <Text key={index} style={styles.title}>
                    {item}
                  </Text>
                )
              })}
            </View>
            <Rating
              showRating
              ratingBackgroundColor='#000'
              onFinishRating={(rating) => ratingCompleted(rating)}
              style={{ paddingVertical: 10}}
            />
          </View>
        </View>
      </>
    )
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    paddingVertical: 20
  },
  titleContainer: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flex: 1
  },
  title: {
    fontSize: 20,
    color: '#000'
  },
  films: {
    fontSize: 20,
    color: '#EEDB00',
    marginTop: 10
  }
})

const mapStateToProps = (state) => {
  console.log(state)
  return {
  }
}

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({ 
    handleRating,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailPerson);

