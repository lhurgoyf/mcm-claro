import React from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import { Text, List } from 'react-native-paper'
import Header from '../components/Header'
import { useState, useEffect } from 'react'
import axios from 'axios'

function ViewPersons({ navigation}, rating) {
  console.log(rating)
  const [person, setPerson] = useState([])

  useEffect(() => {
    axios.get("https://swapi.dev/api/people")
    .then((res) => {
      const response = res.data.results;
      if(response){
        let newState = response.map((e) => e);
        setPerson(newState);
      }
    })
    .catch((error) => {
      console.log(error);
    });
  },[]);

  return (
    <>
      <Header titleText='Star Wars App' />
      <View style={styles.container}>
        <View>
          {person.length === 0 ? (
              <View style={styles.titleContainer}>
                  <Text style={styles.title}>Searching for Jedi's</Text>
              </View>
            ) : (
            <FlatList
              data={person}
              renderItem={({ item, index }) => (
                <List.Item key={index}
                  title={item.name}
                  description={item.gender}
                  descriptionNumberOfLines={1}
                  titleStyle={styles.listTitle}
                  onPress={() => navigation.navigate('DetailPerson', index+1)}
                />
              )}
              keyExtractor={item => item.name}
            />
          )}
        </View>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    titleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    title: {
        fontSize: 20
    },
    listTitle: {
        fontSize: 20
    }
})

export default ViewPersons