
export const Types = {

  GET_RATING: 'swapi/GET_RATING',
  GET_RATING_FAIL: 'swapi/GET_RATING_FAIL',

};

const initialState = {
  rating: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {

    case Types.GET_RATING:
      return {
        ...state,
        rating: action.payload
      };
    case Types.GET_RATING_FAIL:
      return {
        ...state,
        rating: []
      };
    default:
      return state;
  }
}

export async function handleRating(values) {
  return function (dispatch) {
    let total = values;
    console.log(total)
      dispatch({
        type: Types.GET_RATING,
        payload: total
      });
  }
}




