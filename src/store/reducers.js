import { combineReducers } from "redux";

import swapi from './swapi';

const rootReducer = combineReducers({

	swapi,

});

export default rootReducer;
